package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.util.Date;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }

    public List<String> getAllDentists() {
        return entityManager.createQuery("SELECT DISTINCT e.dentistName FROM DentistVisitEntity e").getResultList();
    }

    public DentistVisitEntity getDentistVisit(Long id) {
        return entityManager.find(DentistVisitEntity.class, id);
    }

    public void updateDentistVisit(Long id, String dentistName, Date visitDate, Date visitTime) {
        DentistVisitEntity dentistVisit = getDentistVisit(id);
        dentistVisit.setDentistName(dentistName);
        dentistVisit.setVisitDate(visitDate);
        dentistVisit.setVisitTime(visitTime);
    }

    public void removeDentistVisit(Long id) {
        DentistVisitEntity dentistVisit = getDentistVisit(id);
        if (dentistVisit != null) {
            entityManager.remove(dentistVisit);
        }
    }

    public List<DentistVisitEntity> findDentistVisit(String search) {
        Query query = entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE lower(e.dentistName) LIKE lower(:n)");
        query.setParameter("n", "%" + search + "%");
        return query.getResultList();
    }
}
