package com.cgi.dentistapp.service;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public void addVisit(String dentistName, Date visitDate, Date visitTime) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitDate, visitTime);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    public List<String> listAllDentists() {
        return dentistVisitDao.getAllDentists();
    }

    public DentistVisitEntity findDentistVisitById(Long id) {
        return dentistVisitDao.getDentistVisit(id);
    }

    public void updateDentistVisit(Long id, String dentistName, Date visitDate, Date visitTime) {
        dentistVisitDao.updateDentistVisit(id, dentistName, visitDate, visitTime);
    }

    public void removeDentistVisit(Long id) {
        dentistVisitDao.removeDentistVisit(id);
    }

    public List<DentistVisitEntity> findDentistVisit(String search) {
        return dentistVisitDao.findDentistVisit(search);
    }
}
