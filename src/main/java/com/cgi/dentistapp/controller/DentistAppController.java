package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
        registry.addViewController("/visits").setViewName("visits");
    }

    @ModelAttribute("dentistList")
    public List<String> populateDentists() {
        List<String> dentists = dentistVisitService.listAllDentists();
        List<String> dummyDentists = new ArrayList<>(Arrays.asList("Mari Mänd", "Jüri Kask"));
        for (String dentist : dummyDentists) {
            if (!dentists.contains(dentist)) {
                dentists.add(dentist);
            }
        }
        return dentists;
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "form";
        }

        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitDate(),
                dentistVisitDTO.getVisitTime());
        return "redirect:/results";
    }

    @GetMapping("/visits")
    public String showAllVisits(DentistVisitDTO dentistVisitDTO, Model model) {
        model.addAttribute("allVisits", dentistVisitService.listVisits());
        return "visits";
    }

    @PostMapping("/visits/search")
    public String searchDentistVisit(@RequestParam("search") String searchTerm, Model model) {
        model.addAttribute("searchResults", dentistVisitService.findDentistVisit(searchTerm));
        return "visits";
    }

    @GetMapping("/visits/{id}")
    public String showDentistVisit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("visit", dentistVisitService.findDentistVisitById(id));
        return "visit";
    }

    @GetMapping("visits/{id}/edit")
    public String showUpdateForm(@PathVariable("id") Long id, DentistVisitDTO dentistVisitDTO, Model model) {
        model.addAttribute("visit", dentistVisitService.findDentistVisitById(id));
        return "edit";
    }

    @PostMapping("visits/{id}/edit")
    public String postUpdateForm(@PathVariable("id") Long id, @Valid DentistVisitDTO dentistVisitDTO,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/visits/" + id + "/edit";
        }

        dentistVisitService.updateDentistVisit(id, dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitDate(), dentistVisitDTO.getVisitTime());
        return "redirect:/visits/" + id;
    }

    @DeleteMapping("/visits/{id}/delete")
    public String deleteDentistVisit(@PathVariable("id") Long id) {
        dentistVisitService.removeDentistVisit(id);
        return "redirect:/visits";
    }

}
